(function($){
  $(document).ready(function () {
    $(window).scroll(function(){
      let scrollTop = $(this).scrollTop();
      if(scrollTop > 20) {
        $('.header').addClass('header--scrolled');
      } else {
        $('.header').removeClass('header--scrolled');
      }
    });
    //hide/shw mobile menu
    $('.menu-btn').click(function(){
      let parent = $(this).parent();
      if (parent.hasClass('navigation-opened')) {
        parent.removeClass('navigation-opened').find('.navigation').slideUp();
      } else {
        parent.addClass('navigation-opened').find('.navigation').slideDown();
      }
    });
    // hide/show sub menu
    if(device.desktop()) {
      $('.has-children').hover(function(){
        $(this).addClass('sub-menu-opened').find('.js-drop-content').slideDown();
      }, function(){
        $(this).removeClass('sub-menu-opened').find('.js-drop-content').slideUp();
      });
    }
    if(!device.desktop()) {
      $('.has-children > a').click(function(){
        let parent = $(this).parent();
        if(parent.hasClass('sub-menu-opened')) {
          parent.removeClass('sub-menu-opened').find('.js-drop-content').slideUp();
        } else {
          $('.js-drop-content').slideUp();
          $('.sub-menu-opened').removeClass('sub-menu-opened');
          parent.addClass('sub-menu-opened').find('.js-drop-content').slideDown();
        }
      });
    }
    $('.has-children > a').click(function(e){
      e.preventDefault();
    });

    //home slider
    $('.home-slider').slick({
      arrows: false,
      dots: true,
      slideToShow: 1,
    });

    // hide/show cabinet item
    $('.cabinet-style__item').click(function(e){
      let windowWidth = $(window).width(),
          thisClass   = $(e.target).attr('class'),
          infoHeight  = $(this).find('.cabinet-style__info').height() + 60;
      if(thisClass !== 'cabinet-style__info-close') {
        if(windowWidth > 767) {
          $(this).find('.cabinet-style__info-wrap').addClass('show');
        } else {
          $(this).find('.cabinet-style__info-wrap').height(infoHeight);
        }
      } else {
        if(windowWidth > 767) {
          $(this).find('.cabinet-style__info-wrap').removeClass('show');
        } else {
          $(this).find('.cabinet-style__info-wrap').height(0);
        }
      }
    });

    //dropdown
    $('.js-dropdown__btn').click(function() {
      let parent = $(this).closest('.js-dropdown');
      if (parent.hasClass('opened')) {
        parent.removeClass('opened').find('.js-dropdown__content').slideUp();
      } else {
        parent.addClass('opened').find('.js-dropdown__content').slideDown();
      }
    });

    // accordion
    $('.accordion__btn').click(function() {
      let parent       = $(this).closest('.accordion__item'),
          globalParent = $(this).closest('.accordion');
      if (parent.hasClass('opened')) {
        parent.removeClass('opened').children('.accordion__content').slideUp();
      } else {
        globalParent.children('.accordion__item').removeClass('opened');
        globalParent.find('.accordion__content').slideUp();
        parent.addClass('opened').children('.accordion__content').slideDown();
      }
    });
    if(!device.mobile()) {
      $('.js-first-opened > .accordion__item:first-child').addClass('opened').find('.accordion__content').slideDown();
    }

    // custom input file
    $('input[type="file"]').change(function(){
      let value = $("input[type='file']").val();
      $(this).next('label').text(value);
    });

    // locator, room dropdown
    $('.custom-select__value').click(function(){
      let parent = $(this).parent();
      if(parent.hasClass('active')) {
        parent.removeClass('active').find('.custom-select__drop').slideUp();
      } else {
        parent.addClass('active').find('.custom-select__drop').slideDown();
      }
    });
    $(document).mouseup(function (e){
      let div = $('.custom-select');
      if (!div.is(e.target)
          && div.has(e.target).length === 0) {
        div.removeClass('active').find('.custom-select__drop').slideUp();
      }
    });
    $('.custom-select__btn').click(function(){
      let thisValue      = $(this).text(),
          parent         = $(this).closest('.custom-select'),
          activeValueBox = parent.find('.custom-select__value');
      activeValueBox.html(thisValue);
      parent.removeClass('active').find('.custom-select__drop').slideUp();

      if($(this).hasClass('js-calculator-btn')) {

      }

      if($(this).hasClass('js-select-state')) {
        $('.js-select-content').hide(0);
        $('.js-loader').show(0).delay(1000).fadeOut(1000);

        let thisId = $(this).attr('data-id');

        $('.js-select-content').each(function(){
          let thisContent = $(this).attr('data-content');
          if(thisId === thisContent) {
            $(this).show(0);
          }
        });
      }

    });

    // formatter price
    $("input[data-type='currency']").on({
      keyup: function() {
        const inputName = $(this).attr('name'),
              thisVal = $(this).val(),
              trimmedVal = trimValue(thisVal);

        if (inputName === 'homeValue'){
          const percentVal = ($(this).closest('.budget__item').find('.custom-select__value').html()),
                trimmedPercent = trimValue(percentVal);

          resultBudget(trimmedVal, trimmedPercent);
        } else {
          getBudgetValue(trimmedVal);
        }

        formatCurrency($(this));
      },
      blur: function() {
        formatCurrency($(this), "blur");
      }
    });

    // door-style slider
    $('.portfolio-slider').slick({
      infinite: true,
      centerPadding: '0',
      slidesToShow: 7,
      variableWidth: true,
      centerMode: true,
      responsive: [{
        breakpoint: 1149,
        settings: {
            slidesToShow: 5
          }
        },
        {
        breakpoint: 560,
        settings: {
          slidesToShow: 3
        }
      }]
    });

  })

  function getBudgetValue(inputValue) {
    $('.cabinetsResultValue').text(simpleFormatter(inputValue*0.4));
    $('.flooringResultValue').text(simpleFormatter(inputValue*0.07));
    $('.counterTopsResultValue').text(simpleFormatter(inputValue*0.12));
    $('.appliancesResultValue').text(simpleFormatter(inputValue*0.08));
    $('.electricalResultValue').text(simpleFormatter(inputValue*0.04));
    $('.wallCoveringResultValue').text(simpleFormatter(inputValue*0.03));
    $('.laborResultValue').text(simpleFormatter(inputValue*0.25));
    $('.miscellaneousResultValue').text(simpleFormatter(inputValue*0.01));
    $('.totalRenovationResultValue').text(simpleFormatter(inputValue));
    $('.cabinetRenovationResultValue').text(simpleFormatter(inputValue*0.4));
    $('.estimatedHomeResultValue').text(simpleFormatter(inputValue));
  }

  function simpleFormatter(value) {
    var formatter = new Intl.NumberFormat('en-US', {
      style: 'currency',
      currency: 'USD',
      maximumSignificantDigits: 3
    });

    return formatter.format(value);
  }

  function resultBudget(sum, percent){
    var result = (sum/100)*percent;
    $(".budget__result h5").text(simpleFormatter(result));
    getBudgetValue(result);
  }

  function trimValue(string){
    return Number(string.replace(/[^0-9]/g, ''));
  }

  function formatNumber(n) {
    return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",")
  }

  function formatCurrency(input, blur) {
    let input_val = input.val();
    if (input_val === "") { return; }
    let original_len = input_val.length;
    let caret_pos = input.prop("selectionStart");
    if (input_val.indexOf(".") >= 0) {
      let decimal_pos = input_val.indexOf(".");
      let left_side = input_val.substring(0, decimal_pos);
      let right_side = input_val.substring(decimal_pos);
      left_side = formatNumber(left_side);
      right_side = formatNumber(right_side);
      if (blur === "blur") {
        right_side += "00";
      }
      right_side = right_side.substring(0, 2);
      input_val = "$" + left_side + "." + right_side;
    } else {
      input_val = formatNumber(input_val);
      input_val = "$" + input_val;
      if (blur === "blur") {
        input_val += ".00";
      }
    }
    input.val(input_val);
    let updated_len = input_val.length;
    caret_pos = updated_len - original_len + caret_pos;
    input[0].setSelectionRange(caret_pos, caret_pos);
  }
})(jQuery);
